use super::*;
#[test]
fn builder_new() {
    let want_next_lines = Builder::Lines(28);
    assert_eq!(want_next_lines, Builder::new("28").unwrap());

    let want_repeat = Builder::Repeat(9);
    assert_eq!(want_repeat, Builder::new(r"{9}").unwrap());

    const HEX: &str = r"0[xX]([0-9a-fA-F]+)";
    let want_skip = Builder::Regex {
        re: Regex::new(HEX).unwrap(),
        offset: -2,
        skip: true,
    };

    const INPUT_SKIP: &str = concat!("%", r"0[xX]([0-9a-fA-F]+)", "%", "-2");
    assert_eq!(want_skip, Builder::new(INPUT_SKIP).unwrap());


    // fail: bad offset
    let want_write = Builder::Regex {
        re: Regex::new(HEX).unwrap(),
        offset: -2,
        skip: false,
    };
    const INPUT_WRITE: &str = concat!("/", r"0[xX]([0-9a-fA-F]+)", "/", "-2");
    assert_eq!(want_write, Builder::new(INPUT_WRITE).unwrap());

    let want_fail = "%asijodaoisjdaiosdms/";
    assert!(Builder::new(want_fail).is_err())
}
#[test]
fn find_match_ok() {
    let matcher = Pattern::Match {
        re: Regex::new(r"\w+_[0-9]{3}\.[a-zA-Z0-9]{3}").unwrap(),
        offset: -1,
        skip: false,
        repeat: 1,
    };
    let (start, current) = (0, 3);
    let want = Some((2, true));
    let s = "totally_legit_001.mp3";
    assert_eq!(want, matcher.find(start, current, s));
}
#[test]
fn find_match_too_early() {
    let matcher = Pattern::Match {
        re: Regex::new(r"\w+_[0-9]{3}\.[a-zA-Z0-9]{3}").unwrap(),
        offset: -2,
        skip: false,
        repeat: 1,
    };
    let (start, current) = (1, 2);
    let s = "totally_legit_001.mp3";
    assert_eq!(None, matcher.find(start, current, s))
}
#[test]
fn find_until_line() {
    assert_eq!(Some((20, true)), Pattern::UntilLine(20).find(3, 20, ""));
    assert_eq!(None, Pattern::UntilLine(20).find(2, 19, ""));
}
#[test]
fn find_n_lines() {
    let (start, current) = (8, 15);
    let n = current - start;
    assert_eq!(
        Some((15, true)),
        Pattern::NLines { n, repeat: 1 }.find(start, current, "foobar")
    );
}
