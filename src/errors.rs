#[derive(Debug)]

pub enum Error {
    /// Promotion of an `regex::Error`
    Regex(regex::Error),
    /// A builder argument that doesn't match any pattern
    NoPattern(String),
    /// A technically valid but nonsensical pattern
    BadBuilder(&'static str),
    /// Promotion of an `std::io::Error`
    IO(std::io::Error),
    /// A non-utf8 OsString
    BadString(std::ffi::OsString),
}

pub type Result<T> = std::result::Result<T, Error>;
use std::fmt::{Display, Formatter};
impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Error::Regex(err) => write!(f, "regex error: {}", err),
            Error::NoPattern(arg) => write!(f, "did not recognize {} as a pattern", arg),
            Error::BadBuilder(s) => write!(f, "bad pattern builder: {}", s),
            Error::IO(err) => write!(f, "io error: {}, err", err),
            Error::BadString(s) => write!(f, "could not convert {:?} to utf8 string", s),
        }
    }
}

impl From<regex::Error> for Error {
    fn from(err: regex::Error) -> Self { Error::Regex(err) }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self { Error::IO(err) }
}
