use super::*;

#[test]
fn test_formatter() {
    let mut formatter = Formatter::default();
    formatter.suffix_digits = 4;
    assert_eq!("xx0107", formatter.filename(107));
}
