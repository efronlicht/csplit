# csplit

csplit is a clone of the unix core util `csplit`, but in rust.

## csplit

the csplit utility splits file into pieces using the patterns args. If file is a dash (`'-'`), csplit reads from standard input. The options are as follows:


### args
#### -f prefix

Give created files names beginning with prefix. The default is **"xx"**.

#### -k

Do not remove output files if an error occurs or a HUP, INT or TERM signal is received.

#### -n number

Use number of decimal digits after the prefix to form the file name. The default is 2.

#### -s

Do not write the size of each output file to standard output as it is created.

### patterns:

The args operands may be a combination of the following patterns:

##### `/regexp/[[+|-]offset]


Create a file containing the input from the current line to (but not including) the next line
      matching the given basic regular expression.  An optional offset from the line that matched may
      be specified.


##### `%regexp%[[+|-]offset]`

Same as above but a file is not created for the output.


##### `line_no`


Create containing the input from the current line to (but not including) the specified line
      number.

##### `{num}`

Repeat the previous pattern the specified number of times. If it follows a line number pat tern, a new file will be created for each line_no lines, num times. The first line of the file is line number 1 for historic reasons.

After all the patterns have been processed, the remaining input data (if there is any) will be written to a new file.

## why?

because
